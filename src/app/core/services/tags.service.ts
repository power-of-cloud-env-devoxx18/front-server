import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ConduitApiService } from './conduit-api.service';
import { map } from 'rxjs/operators';

@Injectable()
export class TagsService {
  constructor (
    private apiService: ConduitApiService
  ) {}

  getAll(): Observable<[string]> {
    return this.apiService.get('/tags')
          .pipe(map(data => data.tags));
  }

}
