// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

let userApiUrl = "https://conduit.productionready.io/api";
let conduitApiUrl = "https://conduit.productionready.io/api";

var hostname = window.location.hostname;
if (hostname.includes("-front-devoxx-2018.apps.innershift.sodigital.io")) {
  let appName = hostname.replace("-front-devoxx-2018.apps.innershift.sodigital.io", "");
  userApiUrl = `https//${appName}-user-devoxx-2018.apps.innershift.sodigital.io`;
  conduitApiUrl = `http://${appName}-conduit-devoxx-2018.apps.innershift.sodigital.io`;
}

console.log("Using userApiUrl: ", userApiUrl);
console.log("Using conduitApiUrl: ", conduitApiUrl);

export const environment = {
  production: true,
  user_api_url: userApiUrl,
  conduit_api_url: conduitApiUrl
};
