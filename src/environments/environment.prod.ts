
let userApiUrl = "https://conduit.productionready.io/api";
let conduitApiUrl = "https://conduit.productionready.io/api";

var hostname = window.location.hostname;
if (hostname.includes("-front-devoxx-2018.apps.innershift.sodigital.io")) {
  let appName = hostname.replace("-front-devoxx-2018.apps.innershift.sodigital.io", "");
  userApiUrl = `http://${appName}-user-devoxx-2018.apps.innershift.sodigital.io`;
  conduitApiUrl = `http://${appName}-conduit-devoxx-2018.apps.innershift.sodigital.io`;
}

console.log("Using userApiUrl: ", userApiUrl);
console.log("Using conduitApiUrl: ", conduitApiUrl);

export const environment = {
  production: true,
  user_api_url: userApiUrl,
  conduit_api_url: conduitApiUrl
};
