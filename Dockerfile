FROM devoxx-2018/nginx-image

USER root

COPY . /usr/src/app/

RUN \
    # To support arbitrary user IDs
    chgrp -R 0 /usr/src/app && \
    chmod -R g=u /usr/src/app

EXPOSE 8080

USER 185
